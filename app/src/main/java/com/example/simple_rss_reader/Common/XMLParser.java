package com.example.simple_rss_reader.Common;

import com.example.simple_rss_reader.RSSData.Channel;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class XMLParser {
    public Channel parseXML(String input) {
        Channel parsed;
        parsed = new Channel();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            InputStream is = new ByteArrayInputStream(input.getBytes(Charset.forName("UTF-8")));

            Document doc = builder.parse(is);

            doc.getDocumentElement().normalize();
            System.out.println("Root Element: " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getDocumentElement().getChildNodes();
            Node channelNode = null;
            for(int i = 0; i<nList.getLength(); i++) {
                Node tempNode = nList.item(i);
                System.out.println("|"+tempNode.getNodeName()+"|");
                if(tempNode.getNodeName().equals("channel")) {
                    channelNode = tempNode;
                }
            }

            if(channelNode.getNodeType() == Node.ELEMENT_NODE) {
                nList = channelNode.getChildNodes();
                Element temp = (Element) channelNode;
                parsed.setTitle(temp.getElementsByTagName("title").item(0).getTextContent());
                parsed.setLink(temp.getElementsByTagName("link").item(0).getTextContent());

                ArrayList<Channel.RSSItem> items = new ArrayList<>();

                for(int i = 0; i<nList.getLength(); i++) {
                    Node tempNode = nList.item(i);
                    if (tempNode.getNodeName().equals("item")) {
                        if(tempNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element item = (Element) tempNode;
                            items.add(new Channel.RSSItem(
                                    item.getElementsByTagName("title").item(0).getTextContent(),
                                    item.getElementsByTagName("link").item(0).getTextContent(),
                                    item.getElementsByTagName("description").item(0).getTextContent(),
                                    item.getElementsByTagName("pubDate").item(0).getTextContent()));
                        }
                    } else if(tempNode.getNodeName().equals("image")) {
                        if(tempNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element image = (Element) tempNode;
                            parsed.setImage(new Channel.RSSImage(
                                    image.getElementsByTagName("title").item(0).getTextContent(),
                                    image.getElementsByTagName("url").item(0).getTextContent(),
                                    image.getElementsByTagName("link").item(0).getTextContent()));
                        }
                    }
                }
                parsed.setItems(items);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return parsed;
    }
}
