package com.example.simple_rss_reader.RSSAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.simple_rss_reader.Interface.ItemClickListener;
import com.example.simple_rss_reader.R;
import com.example.simple_rss_reader.RSSData.Channel;

class FeedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener {

    public TextView txtTitle, txtPubDate, txtContent;
    private ItemClickListener itemClickListener;

    public FeedViewHolder(View view) {
        super(view);

        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtPubDate = (TextView) view.findViewById(R.id.txtPubDate);
        txtContent = (TextView) view.findViewById(R.id.txtContent);

        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), true);
        return true;
    }
}

public class FeedAdapter extends RecyclerView.Adapter<FeedViewHolder>{
    private Channel channel;
    private Context mContext;
    private LayoutInflater layoutInflater;

    public FeedAdapter(Channel channel, Context Context) {
        this.channel = channel;
        this.mContext = Context;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.rss_row, parent, false);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder feedViewHolder, int position) {
        feedViewHolder.txtTitle.setText(channel.getItems().get(position).getTitle());
        feedViewHolder.txtPubDate.setText(channel.getItems().get(position).getPubDate());
        feedViewHolder.txtContent.setText(channel.getItems().get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return channel.getItems().size();
    }
}
