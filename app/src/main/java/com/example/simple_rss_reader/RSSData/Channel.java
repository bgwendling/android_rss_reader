package com.example.simple_rss_reader.RSSData;

import java.util.ArrayList;

public class Channel {
    public static class RSSImage {
        private String title;
        private String url;
        private String Link;

        public RSSImage(String title, String url, String link) {
            this.title = title;
            this.url = url;
            Link = link;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLink() {
            return Link;
        }

        public void setLink(String link) {
            Link = link;
        }
    }

    public static class RSSItem {
        private String title;
        private String link;
        private String description;
        private String pubDate;

        public RSSItem(String title, String link, String description, String pubDate) {
            this.title = title;
            this.link = link;
            this.description = description;
            this.pubDate = pubDate;
        }

        public RSSItem() {

        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPubDate() {
            return pubDate;
        }

        public void setPubDate(String pubDate) {
            this.pubDate = pubDate;
        }
    }

    private String title;
    private String link;
    private String description;
    private RSSImage image;
    private ArrayList<RSSItem> items;

    public Channel() {
    }

    public Channel(String title, String link, String description, RSSImage image, ArrayList<RSSItem> items) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.image = image;
        this.items = items;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RSSImage getImage() {
        return image;
    }

    public void setImage(RSSImage image) {
        this.image = image;
    }

    public ArrayList<RSSItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<RSSItem> items) {
        this.items = items;
    }
}
